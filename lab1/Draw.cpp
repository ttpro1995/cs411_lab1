/*
Thai Thien
1351040

*/
#include "Draw.h"
#include <cmath>
#include <iostream>
#include <GL\freeglut.h>


const float Draw::scale_unit = 1;
void Draw::drawLineWithDDA(float x0, float y0, float x1, float y1) {
	glColor3f(0.5f,1,0.8f);
	int dx = x1 - x0;
	int dy = y1 - y0;
	float x = x0;
	float y = y0;
	int steps;
	if (abs(dx) > abs(dy))
		steps = abs(dx);
	else
	{
		steps = abs(dy);
	}
	
	steps = steps / scale_unit;

	float xInc = dx / (float)steps;
	float yInc = dy / (float)steps;

	Draw::putpixel(x, y);
	for (int i = 0; i < steps; i++) {
		x += xInc;
		y += yInc;
		Draw::putpixel(x, y);
	}

}

void Draw::drawCircleWithMidpoint(float x0, float y0, float r) {
	glColor3f(0.8f, 0.3f, 0.3f);

	float x = r;
	float y = 0;


	// decision = 2[(x*x + y*y - r*r ) + (2y+1)] + 1 - 2x
	
	// 2[(r*r + 0*0 - r*r)+ (2*0 +1)] + 1 - 2*r = 3 - 2*r
 	float decision = 3 - 2*x;   // Decision criterion divided by 2 evaluated at x=r, y=0

	while (y <= x)
	{
		putpixel(x + x0, y + y0); // Octant 1
		putpixel(y + x0, x + y0); // Octant 2
		putpixel(-y + x0, x + y0); // Octant 3
		putpixel(-x + x0, y + y0); // Octant 4
		putpixel(-x + x0, -y + y0); // Octant 5
		putpixel(-y + x0, -x + y0); // Octant 6
		putpixel(x + x0, -y + y0); // Octant 7
		putpixel(y + x0, -x + y0); // Octant 8
		y+= scale_unit;
		if (decision <= 0)
		{
			/*
			 decision = f(x, y+1) - f(x, y) - 4 ( we -4 because new y is already ++)
			*/
			decision += 4 * y + (6 - 4)* scale_unit;   // Change in decision criterion for y -> y+1
		}
		else
		{

			/*
			decision = f(x - 1, y+1) - f(x, y) - 4 ( we -8 because new y, x have been increase/decrease)
			*/
			x-= scale_unit;
			decision += 4 * (y - x) + (10 - 8)* scale_unit;   // Change for y -> y+1, x -> x-1
		}
	}
}



void Draw::drawLineWithBresenham(float x0, float y0, float x1, float y1) {
	glColor3f(255.0f / 255.0f, 255.0f / 255.0f, 0);

	float dx = abs(x0 - x1);
	float dy = abs(y0 - y1);
	float p = 2 * dy - dx;
	float twoDy = 2 * dy ;
	float twoDyDx = 2 * (dy - dx) ;
	float x, y, xEnd;

	// set start and end point
	if (x0 > x1) {
		x = x1;
		y = y1;
		xEnd = x0;
	}
	else {
		x = x0;
		y = y0;
		xEnd = x1;
	}
	putpixel(x, y);
	while (x < xEnd) {
		x+= scale_unit;
		if (p < 0)
			p += twoDy;
		else {
			y += scale_unit;
			p += twoDyDx;
		}
		putpixel(x, y);
	}

}

void Draw::drawEllipseWithMidpoint(float x0, float y0, float rx, float ry) {
	glColor3f(0 / 255.0f, 102.0f / 255.0f, 255.0f/255.0f);

	float x = 0;
	float y = ry;

	// decision = ry^2 - rx^2 * ry + (1/4) * rx^2
	float decision = ry*ry - rx*rx*ry + 0.25*rx*rx;
	while (2*ry*ry*x < 2*rx*rx*y)
	{
		putpixel(x + x0, y + y0);
		putpixel(-x + x0, y + y0);
		putpixel(x + x0, -y + y0);
		putpixel(-x + x0, -y + y0);

		
		
		if (decision < 0) {
			x += scale_unit;
			decision +=  2*ry*ry*x + ry*ry;
		}
		else {
			x += scale_unit;
			y -= scale_unit;
			decision += 2 * ry*ry*x - 2*rx*rx*y + ry*ry;
		}
	}

	decision = 0;

	while (y>0) {
		putpixel(x + x0, y + y0);
		putpixel(-x + x0, y + y0);
		putpixel(x + x0, -y + y0);
		putpixel(-x + x0, -y + y0);

		if (decision > 0) {
			y -= scale_unit;
			decision += -2 * rx*rx*y + rx*rx;
		}
		else {
			x += scale_unit;
			y -= scale_unit;
			decision += 2 * ry*ry*x - 2 * rx*rx*y + rx*rx;
		}
	}
}

void Draw::drawParabolaWithMidpoint(float x0, float y0, float a) {
	glColor3f(1.0f, 1.0f, 1.0f);

	// y^2 = ax
	// y*y - ax = 0 
	// decision = y*y - ax
	float y = 0;
	float x = 0;
	float decision = 0;
	float threshold = 200.0f;
	while (y < threshold) {

		putpixel(x + x0, y + y0);
		putpixel(x + x0, -y + y0);

		//cong x
		// be hon 0 thi cong y
		x += scale_unit;
		if (decision > 0) {
			decision -= a*scale_unit;
		}
		else {
			y += scale_unit;
			decision = 2 * y*scale_unit + scale_unit*scale_unit - 2 * scale_unit - a*scale_unit;// (y + 1)^2 - a(x-1) - ( y*y - ax) = 2*y + 1 - a, minus 2 for each y
		}
	}



}

void Draw::drawHyperbolaWithMidpoint(float xt, float yt, float a, float b) {
	// (xx/aa)-(yy/bb) = 1
	// let decision function = xx/aa - yy/bb - 1
	glColor3f(0, 1.0f, 0);
	float threshold = 200.0f;

	//start point (0, a)
	float x = a;
	float y = 0;

	float decision = 0;
	while (y < threshold) {
		putpixel(x + xt, y + yt);
		putpixel(x + xt, -y + yt);
		putpixel(-x + xt, y + yt);
		putpixel(-x + xt, -y + yt);
		y += scale_unit;
		if (decision > 0) {
			decision -= (2 * y + 1 - 2) / (b*b*scale_unit);
		}
		else {
			x += scale_unit;
			decision = scale_unit*((2 * x + 1 - 2) / (a*a) - (2 * y + 1 - 2) / (a*a));
		}
	}
}

void Draw::putpixel(float x, float y) {
	glVertex3f(x, y, 0.0f);
}


Draw::Draw()
{
}


Draw::~Draw()
{
}
