/*
Thai Thien
1351040

*/
#pragma once
class Draw
{
public:
	// because the point is too small, for charity, let increase the point appear on canvas
	// scale_unit = 1 -> one point each canvas unit. Reduce to 0.2 will x5 number of point
	static const float scale_unit;
	
	 
	Draw();
	~Draw();
	static void drawLineWithDDA(float x0, float y0, float x1, float y1);
	static void drawLineWithBresenham(float x0, float y0, float x1, float y1);
	static void putpixel(float x, float y);
	static void drawCircleWithMidpoint(float x, float y, float r);
	static void drawEllipseWithMidpoint(float x0, float y0, float rx, float ry);
	static void drawParabolaWithMidpoint(float x0, float y0, float a);
	static void drawHyperbolaWithMidpoint(float xt, float yt, float a, float b);

};

