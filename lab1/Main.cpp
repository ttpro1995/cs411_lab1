/*
Thai Thien
1351040

*/

#include <GL\glew.h>
#include<GL\freeglut.h>



#include <cmath>
#include <iostream>
#include "Draw.h"
#include <fstream>
#include <string>
#include <ctime>

using namespace std;
void readfile();

void display(void)
{
	/* clear all pixels */
	glClear(GL_COLOR_BUFFER_BIT);

	glColor3f(0.0f, 1.0f, 1.0f); 
	//glDrawArrays(GL_POINTS, 0, 12);
	glPointSize(1);

	//
	//glVertex3f(25.0f, 25.0f, 0.0f);
	// (25, 25) (25,10), (10,10)
	//Draw::drawLineWithBresenham(25.0f, 25.0f, 10.0f, 10.0f);
	//Draw::drawLineWithDDA(25.0f, 25.0f, 25.0f, 10.0f);
	//Draw::drawLineWithBresenham(25.0f, 10.0f, 10.0f, 10.0f);
	//Draw::drawCircleWithMidpoint(15.0f, 15.0f, 10.0f);
	//Draw::drawEllipseWithMidpoint(10.0f, 10.0f, 5.0f, 2.0f);
	readfile();
}

void readfile() {
	ifstream fin;
	fin.open("input.txt");
	int objType;
	while (fin >> objType) {
		GLuint64 startTime, stopTime;
		unsigned int queryID[2];
		std::string name="";
		// generate two queries
		glGenQueries(2, queryID);
		glQueryCounter(queryID[0], GL_TIMESTAMP);

		glBegin(GL_POINTS);

		float x1, y1, x2, y2;
		float xt, yt, r, a, b;
		float p;
		switch (objType)
		{
		case 0:
			fin >> x1>> y1>> x2>> y2;
			Draw::drawLineWithDDA(x1, y1, x2, y2);
			name = "Draw line with DDA";
			break;
		case 1:
			fin >> x1>> y1>> x2>> y2;
			Draw::drawLineWithBresenham(x1, y1, x2, y2);
			name = "Draw line with Bresenham";
			break;
		case 2:
			fin >> xt >> yt >> r;
			Draw::drawCircleWithMidpoint(xt, yt, r);
			name = "Draw Circle With Midpoint";
			break;
		case 3:
			fin >> xt >> yt >> a >> b;
			Draw::drawEllipseWithMidpoint(xt, yt, a, b);
			name = "draw Ellipse With Midpoint";

			break;
		case 4:
			fin >> xt >> yt >> p;
			Draw::drawParabolaWithMidpoint(xt, yt, p);
			name = "Draw Parabola With Midpoint";
			break;
		case 5:
			fin >> xt >> yt >> a >> b;
			Draw::drawHyperbolaWithMidpoint(xt, yt, a, b);
			name = "Draw Hyperbola With Midpoint";
			break;
		default:
			break;
		}
		glEnd();

		glQueryCounter(queryID[1], GL_TIMESTAMP);
		// wait until the results are available
		GLint stopTimerAvailable = 0;
		while (!stopTimerAvailable) {
			glGetQueryObjectiv(queryID[1],
				GL_QUERY_RESULT_AVAILABLE,
				&stopTimerAvailable);
		}

		// get query results
		glGetQueryObjectui64v(queryID[0], GL_QUERY_RESULT, &startTime);
		glGetQueryObjectui64v(queryID[1], GL_QUERY_RESULT, &stopTime);
		cout <<  name + " ";
		//printf(" in : %f ms\n", (stopTime - startTime));
		printf(" in : %f ms\n" ,(stopTime - startTime) / 1000000.0);
	}
	glFlush();
	cout << "Done drawing a scene---------------- \n";
}

void init(void)

{

	/* select clearing color */

	glClearColor(0, 0, 0, 0.0);

	/* initialize viewing values */

	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();

	//glOrtho(0.0, 30.0, 0.0, 35.0, -1.0, 1.0);
	gluOrtho2D(0, 500, 0, 500);
}

int main(int argc, char** argv)

{


	glutInit(&argc, argv);



	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	glutInitWindowSize(500, 500);

	glutInitWindowPosition(100, 100);
	glutInitContextVersion(1, 5);
	glutCreateWindow("1351040");
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		/* Problem: glewInit failed, something is seriously wrong. */
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
	}

	init();



	glutDisplayFunc(display);


	glutMainLoop();

	return 0; /* ANSI C requires main to return an int. */

}